import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { By } from '@angular/platform-browser';

import { TaskListComponent } from './task-list.component';

import { TaskServiceStub } from 'src/app/services/mocks/task.service.mock';
import { TaskService } from 'src/app/services/task.service';

describe('TaskListComponent', () => {
  let component: TaskListComponent;
  let fixture: ComponentFixture<TaskListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TaskListComponent],
      imports: [HttpClientModule, AppRoutingModule],
      providers: [
        {
          provide: TaskService,
          useClass: TaskServiceStub,
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Debería renderizar la lista de TODOS en el HTML', (done) => {
    fixture.whenStable().then(() => {
      fixture.detectChanges()
      const elemento = fixture.debugElement.query(By.css('.card')).nativeElement;
      expect(elemento.childNodes[1]).not.toBeNull();
    })
    done()
  });


});
