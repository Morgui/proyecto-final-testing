import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ITask } from 'src/app/models/task.interface';
import { TaskService } from 'src/app/services/task.service';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss'],
})
export class TaskListComponent implements OnInit {
  @Input() tasks: ITask[] = [];

  constructor(public taskService: TaskService, private router: Router) {}

  ngOnInit(): void {}

  delete(id: number | undefined) {
    console.log(`borrando el id ${id}`);
    if (id) {
      this.taskService.delete(id).subscribe((response: any) => {
        console.log(response)
        this.router.navigate(['/']);
      });
    } else {
      console.warn('No se ha podido borrar la tarea');
    }
  }
}
