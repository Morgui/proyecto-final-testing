import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ITask } from 'src/app/models/task.interface';
import { TaskService } from 'src/app/services/task.service';

@Component({
  selector: 'app-create-task-form',
  templateUrl: './create-task-form.component.html',
  styleUrls: ['./create-task-form.component.scss']
})
export class CreateTaskFormComponent implements OnInit {

  constructor( private formBuilder: FormBuilder, private router: Router, private taskService: TaskService) { }

  task: ITask = {
    title: '',
    due_on: new Date(),
    status: '',
  }
  form: FormGroup = this.formBuilder.group({});
  error: any;


  ngOnInit(): void {
    this.form = this.formBuilder.group({
      title: [this.task.title, Validators.required],
      user_id: [this.task.user_id],
      due_on: [this.task.due_on, Validators.required],
      status: [this.task.status, Validators.required],
    });
  }

  create(){
    console.log(this.task)
    this.taskService.create(this.task).subscribe((response:any)=>{
      console.log(response)
      if(response['id']){
        this.router.navigate(['/']);
      }else{
        this.error = ['No se ha podido crear la tarea']
      }
    });
  }

}
