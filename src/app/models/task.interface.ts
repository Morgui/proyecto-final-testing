export interface ITask {
  id?: number;
  user_id?: number;
  title: string;
  due_on: Date;
  status: string;
}
