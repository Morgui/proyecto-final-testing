import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';

import { TaskViewComponent } from './task-view.component';
import { TaskService } from 'src/app/services/task.service';
import { TaskServiceStub } from 'src/app/services/mocks/task.service.mock';

describe('TaskViewComponent', () => {
  let component: TaskViewComponent;
  let fixture: ComponentFixture<TaskViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TaskViewComponent],
      imports: [HttpClientModule],
      providers: [
        {
          provide: TaskService,
          useClass: TaskServiceStub,
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Se debe llamar a getTaskList del Servicio en ngOnInit', () => {
    spyOn(component.taskService, 'getTaskList').and.callThrough();

    component.ngOnInit();

    expect(component.taskService.getTaskList).toHaveBeenCalledTimes(1);
  });

  it('Deberia obtener la lista de TODOS y guardarla', (done)  => {
    fixture.whenStable().then(() => {
      expect(component.tasks.length).toBeGreaterThan(0);
    })
    done();
  });
});
