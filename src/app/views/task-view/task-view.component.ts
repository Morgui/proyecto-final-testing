import { Component, OnInit } from '@angular/core';
import { ITask } from 'src/app/models/task.interface';
import { TaskService } from 'src/app/services/task.service';

@Component({
  selector: 'app-task-view',
  templateUrl: './task-view.component.html',
  styleUrls: ['./task-view.component.scss'],
})
export class TaskViewComponent implements OnInit {
  constructor(public taskService: TaskService) {}

  tasks: ITask[] = [];

  ngOnInit(): void {
    this.taskService.getTaskList().subscribe((response: any) => {
      console.table(response.data);
      this.taskService.setTasks(response.data)
      this.tasks = response.data;
    }),
      (error: any) => {
        console.warn(error);
      };
  }
}
