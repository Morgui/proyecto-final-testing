import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotFoundPageComponent } from './pages/not-found-page/not-found-page.component';
import { TaskListPageComponent } from './pages/task-list-page/task-list-page.component';
import { CreateTaskPageComponent } from './pages/create-task-page/create-task-page.component'; 

const routes: Routes = [
  {
    //localhost:4200
    path: '',
    component: TaskListPageComponent,
    pathMatch: 'full',
  },
  {
    //localhost:4200/tasks/create
    path: 'tasks/create',
    component: CreateTaskPageComponent,
  },
  {
    //localhost:4200/"paginaInexistente"
    path: '**',
    component: NotFoundPageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
