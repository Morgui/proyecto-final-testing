import { Observable, of } from 'rxjs';
import { ITask } from 'src/app/models/task.interface';

export class TaskServiceStub {
  
  getTaskList(): Observable<any> {
    return of({
      data: [
        {
          id: 17,
          user_id: 14,
          title:
            'Vicinus id absque talio cultellus vetus capillus chirographum explicabo addo.',
          due_on: '2021-10-10T00:00:00.000+05:30',
          status: 'pending',
        },
        {
          id: 18,
          user_id: 14,
          title:
            'Utrum est et aureus excepturi strues thesaurus cunctatio conatus curvus.',
          due_on: '2021-09-27T00:00:00.000+05:30',
          status: 'completed',
        },
      ],
    });
  }

  create(task: ITask): Observable<ITask>{
      return of({
        id: 17,
        user_id: 14,
        title:
          'Vicinus id absque talio cultellus vetus capillus chirographum explicabo addo.',
        due_on: new Date(),
        status: 'pending',
      })
  }

  delete(id:number): Observable<boolean>{
    return of(true)
  }

  setTasks(tasks: ITask[]){
    return null
  }
}
