import { getTestBed, TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { TaskService } from './task.service';

const stubTasks = {
  data: [
    {
      id: 17,
      user_id: 14,
      title:
        'Vicinus id absque talio cultellus vetus capillus chirographum explicabo addo.',
      due_on: '2021-10-10T00:00:00.000+05:30',
      status: 'pending',
    },
    {
      id: 18,
      user_id: 14,
      title:
        'Utrum est et aureus excepturi strues thesaurus cunctatio conatus curvus.',
      due_on: '2021-09-27T00:00:00.000+05:30',
      status: 'completed',
    },
  ],
};

const stubCreateTask = {
  due_on: new Date('2021-09-25'),
  id: 0,
  status: 'pending',
  title: 'create task title ',
  user_id: 5,
};

describe('TaskService', () => {
  let service: TaskService;
  let testBed: TestBed;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [TaskService],
    });

    testBed = getTestBed();
    httpMock = testBed.inject(HttpTestingController);
    service = TestBed.inject(TaskService);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  /**
   * Test para obtener la lista de tareas
   */

  it('Debería obtener la lista de tareas', () => {
    service.getTaskList().subscribe((response) => {
      expect(response).toBe(stubTasks);
    });

    const peticion = httpMock.expectOne('https://gorest.co.in/public/v1/todos');
    peticion.flush(stubTasks);
    expect(peticion.request.method).toBe('GET');
  });

  it('Debería crear una tarea', () => {
    service
      .create({
        due_on: new Date('2021-09-25'),
        id: undefined,
        status: 'pending',
        title: 'create task title ',
        user_id: 5,
      })
      .subscribe((response) => {
        expect(response).toEqual(stubCreateTask);
      });

  });

  it('Debería borrar una tarea', () => {
    service.delete(1).subscribe((response)=>{
      expect(response).toBeFalse();
    })
  });
});
