import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ITask } from '../models/task.interface';


@Injectable({
  providedIn: 'root',
})
export class TaskService {
  taskDataBase: ITask[] = [];

  constructor(private http: HttpClient) {}

  setTasks(tasks: ITask[]){
    this.taskDataBase = tasks;
  }

  getTaskList(): Observable<any> {
    if(this.taskDataBase.length == 0){
      return this.http.get('https://gorest.co.in/public/v1/todos')
    }
    return of({data: this.taskDataBase})
  }


  create(task: ITask): Observable<ITask> {
    let last: ITask = this.taskDataBase[this.taskDataBase.length - 1];
    let newId: number = 0
    if(last == undefined){
       newId = 0;
    }else{
       newId = last.id == undefined ? 0 : last.id + 1;

    }
    task.id = newId;
    this.taskDataBase.unshift(task);
    return of(task);
  }

  delete(id: number): Observable<boolean> {
    let position: number = -1;

    for (let index = 0; index < this.taskDataBase.length; index++) {
      console.log(this.taskDataBase[index].id, id)
      if (this.taskDataBase[index].id == id) {
        position = index;
        break;
      }
    }

    if (position == -1) {
      return of(false);
    }
    this.taskDataBase.splice(position, 1);
    return of(true);
  }
}
