describe('Pruebas de crear y borrar tareas', () => {

    it('Crear Tarea', () => {
      cy.visit('https://proyecto-final-testing.netlify.app/').then(() => {
        cy.contains('Crear Tarea').click();
        cy.get('#mat-input-0').type(17);
        cy.get('#mat-input-1').type("Test para cypress");
        cy.get('#mat-input-2').type("2021-09-24");
        cy.get('#mat-input-3').type("pending");
        cy.contains('Crear Tarea').click();
      });
    });


    it('Borrar Tarea', () => {
        cy.visit('https://proyecto-final-testing.netlify.app/').then(() => {
            cy.get('#delete').should('have.text', 'Borrar').click();
         });
      });

  
  });
